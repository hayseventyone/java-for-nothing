package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        Not use Generic
//        List myList = new ArrayList();
//        myList.add("string");
//        myList.add(12);
//        myList.add(new Date());
//
//        int s = (int) myList.get(1);

        List<Integer> myList = new ArrayList<>();

        myList.add(12);
        myList.add(new  Integer("2312"));

//        System.out.println(myList.get(0));
//        System.out.println(myList.get(1));
////
//        for (Integer x : myList){
//            System.out.println(x);
//        }
//
//
//        List<Customer> myCust = new ArrayList<>();
//        myCust.add(new Customer("1","besz","203/3 suttisan"));
//        myCust.add(new Customer("2","psoa","12/3 reapdoawn"));
//
//
//        myCust.add(1,new Customer("3","ggex® ","phitsanulok"));
//        Customer cust = myCust.get(1);
////        System.out.println(cust.getAddr());
//        myCust.remove(myCust.size()-1);
//
//        for (Customer c :myCust){
//            System.out.println(c.getName());
//            System.out.println(c.getAddr());
//        }


        Set<String> mySet = new HashSet();
        mySet.add("ss");//ไม่ออกก เพราะ add ซ่ำตอนท้าย 49
        mySet.add("xfsd");
        mySet.add("tetst");
        mySet.add("ss");

        for (String str :mySet){
            System.out.println(str);
        }

        String [] strs = mySet.toArray(new String[0]);

        for(String str : strs){
            System.out.println(str);

        }

        Map<String,Customer> myMap = new HashMap<>();
        myMap.put("1", new Customer("1","besz","203/3 suttisan"));
        myMap.put("2",new Customer("2","besz","203/3 suttisan"));
        myMap.put("2",new Customer("3","asd","13/3 ccccc"));
        System.out.println(myMap);
        System.out.println(myMap.get("2").getAddr());

        for (String key :myMap.keySet()){
            System.out.println(key);
        }

        Collection<Customer> allCust = myMap.values();
        for (Customer c :allCust){
            System.out.println(c.getId());
            System.out.println(c.getName());

        }
    }
}


class Customer{
     private String id;
     private String name;
     private String addr;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public Customer(String id, String name, String addr) {
        this.id = id;
        this.name = name;
        this.addr = addr;
    }
}
