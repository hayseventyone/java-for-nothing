package com.yannv.api.demo;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * CustomerController
 */
@RestController
public class CustomerController {

    @Autowired
    CustomerService customerService;

    // @RequestMapping("/customers") // geturl
    @GetMapping("/customers")
    public List<Customer> getAllCust() {
        return customerService.retriveCustomer();
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<?> getCustomer(@PathVariable Long id) {
        Optional<Customer> customer = customerService.retriveCustomer(id);
        if (!customer.isPresent()) {
            return ResponseEntity.notFound().build();

        }
        return ResponseEntity.ok((customer));
    }

    @PostMapping("/customers")
    public ResponseEntity<?> postCustomer(@Valid @RequestBody Customer cust) {
        Customer newCustomer = customerService.createCustomer(cust);
        return ResponseEntity.status(HttpStatus.CREATED).body(newCustomer);
    }

    @PutMapping({ "/customers/{id}" })
    public ResponseEntity<?> updateCustomer(@PathVariable long id, @Valid @RequestBody Customer cust) {
        Optional<Customer> customer = customerService.updateCustomer(id, cust);
        if (!customer.isPresent()) {
            return ResponseEntity.notFound().build();

        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping({ "/customers/{id}" })
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
        if (!customerService.deleteCustomer(id)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }

}
