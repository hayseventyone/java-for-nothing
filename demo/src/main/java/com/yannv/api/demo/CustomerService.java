package com.yannv.api.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * CustomerService
 */
@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository repo) {
        this.customerRepository = repo;
    }

    @Transactional
    public List<Customer> retriveCustomer() {
        return (List<Customer>) customerRepository.findAll();
    }

    @Transactional
    public Optional<Customer> retriveCustomer(long id) {
        return customerRepository.findById(id);
    }

    @Transactional
    public Customer createCustomer(Customer cust) {
        cust.setId(null);
        return customerRepository.save(cust);
    }

    @Transactional
    public Optional<Customer> updateCustomer(Long id, Customer cust) {
        Optional<Customer> customerOptional = customerRepository.findById(id);
        if (!customerOptional.isPresent()) {
            return customerOptional;
        }
        cust.setId(id);
        return Optional.of(customerRepository.save(cust));

    }

    @Transactional
    public boolean deleteCustomer(Long id) {
        try {
            customerRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;

        }

    }

}