package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Student std = new Student("1", "Yanavat", "benzpopza@hotmail.com", "089-9283920");
//        Course Java = new Course("1", "Java", "5", 190000);
//
        Student std2 = new Student("2","Benz","benza2244@hotmail.com", "027127000");

        ProgramingCourse progC = new ProgramingCourse("P1","Java SE 8","6",100000,"Java");

        DatabaseCourse dbC = new DatabaseCourse("P2","Microsoft SQL Server","6",12000);


       std2.addCourse(progC);
       std2.addCourse(dbC);

        for (Course allCourse : std2.getAllCourse()) {
            System.out.println(std2.getName());
            System.out.println(allCourse.getTitle());
            System.out.println(allCourse.getDuration());
            System.out.println(allCourse.getPirce());

        }

//
//       // std2.setStCourse(progC);
//        System.out.println(std2.getName());
//        System.out.println(std2.getStCourse().getTitle());
//        System.out.println(std2.getStCourse().getDuration());
//        System.out.println(std2.getStCourse().getPirce());
//        System.out.println(std2.getStCourse());
//
//      //  std2.setStCourse(dbC);
//        System.out.println(std2.getName());
//        System.out.println(std2.getStCourse().getTitle());
//        System.out.println(std2.getStCourse().getDuration());
//        System.out.println(std2.getStCourse().getPirce());


        //test SingleTon

        TrainingManager testManager = TrainingManager.getInstance();

        Student s1 =  testManager.createNewStudent("1","benz","benz@hotmail.com","02-7127000");

        Student s2 =  testManager.createNewStudent("1","benz","benz@hotmail.com","02-7127000");

        Student s3 =  testManager.createNewStudent("1","benz","benz@hotmail.com","02-7127000");
//        testManager.createNewStudent(new Student("1","benz","benz@hotmail.com","02-7127000"));
        testManager.createNewCourse(new Course("3","Java SE 11","5",10000));

        testManager.createNewCourse(new Course("4","Java SE 12","2",10002));



        int totalCourse = testManager.getTotalCourse();
        int totalStudent = testManager.getTotalStudent();

        System.out.println(totalCourse);
        System.out.println(totalStudent);

    }
}