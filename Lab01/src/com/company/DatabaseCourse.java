package com.company;

public class DatabaseCourse extends Course {
    private String typeDb ;

    public DatabaseCourse(String id, String title, String duration, float pirce) {
        super(id, title, duration, pirce);
    }

    public String getTypeDb() {
        return typeDb;
    }

    public void setTypeDb(String typeDb) {
        this.typeDb = typeDb;
    }
}
