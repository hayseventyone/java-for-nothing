package com.company;

public class Course {
    private String id;
    private String title ;
    private String duration;
    private float pirce ;

    public Course() {
    }

    public Course(String id, String title, String duration, float pirce) {
        this.id = id;
        this.title = title;
        this.duration = duration;
        this.pirce = pirce;
    }

    public Course(Course std) {
        this(std.id,std.title,std.duration,std.pirce);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public float getPirce() {
        return pirce;
    }

    public void setPirce(float pirce) {
        this.pirce = pirce;
    }
}
