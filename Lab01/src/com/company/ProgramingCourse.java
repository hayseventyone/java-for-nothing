package com.company;

public class ProgramingCourse extends Course {
    private String Language;

    public ProgramingCourse(String id, String title, String duration, float pirce, String language) {
        super(id, title, duration, pirce);
        Language = language;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }
}
