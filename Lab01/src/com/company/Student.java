package com.company;

import java.util.ArrayList;
import java.util.List;

public class Student {


    private String id;
    private String name;
    private String lastName;
    private String email;
    private String tel;
    private double grade;
    private Course stCourse;
    private List<Course> myCourses;

    public Student() {


    }

    public Student(String id, String name, String email, String tel) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.tel = tel;

        List<Course> myCourses = new ArrayList<>();
        this.myCourses = myCourses;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public void addCourse(Course c) {
        myCourses.add(c);

    }

    public List<Course> getAllCourse() {
        return myCourses;


    }

}
