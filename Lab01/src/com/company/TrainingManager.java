package com.company;

//implement Singleton; Factory
public class TrainingManager {
    private static int TotalCourse =0;
    private static int TotalStudent=0;
    private static TrainingManager instance = new TrainingManager();

    private TrainingManager() {
    }

    public static TrainingManager getInstance (){ return instance; }

    public Student createNewStudent(String id, String name, String email, String tel){
        TotalStudent++;
        return  new Student(id,name,email,tel);

    }

    public Course createNewCourse(String id, String title, String duration, float pirce){
        TotalCourse++;
        return new Course(id, title,duration,pirce);
    }

    public static int getTotalCourse() {
        return TotalCourse;
    }

    public static int getTotalStudent() {
        return TotalStudent;
    }

    public void createNewCourse(Course std) {
        TotalCourse++;
        Course C1 = new Course(std);

    }
}
