@FunctionalInterface
public interface MyOperator {
    public double operator(double a , double b);
}
