

import java.util.List;
import model.Customer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author home
 */
public interface CustomerDAOInf {
       public List<Customer> getAllCustomer();
       public void addCustomer(Customer NewCust);
       public void updateCustomer(Customer old ,Customer newCust);
       public void deleteCustomer(Customer old ,Customer newCust);
       public Customer getCustomer(String id);
    
}
