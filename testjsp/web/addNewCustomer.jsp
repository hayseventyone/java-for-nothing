<%-- 
    Document   : addNewCustomer
    Created on : Jun 21, 2019, 12:04:40 AM
    Author     : home
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            button {
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                cursor: pointer;
                width: 100%;
            }
            
button:hover {
  opacity: 0.8;
}

            @media screen and (max-width: 300px) {
                span.psw {
                    display: block;
                    float: none;
                }
                .cancelbtn {
                    width: 100%;
                }
            }
            input[type=text], input[type=password] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                box-sizing: border-box;
            }          
            .container {
                padding: 16px;

            }
        </style>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <form action="/newCustController">
                <label for = "id"><b>ID</b></label>
                <input type ="text" id = "id" name ="id" placeholder="Customer  ID">
                <label for ="name"><b>Name</b></label>
                <input type ="text" id = "name" name="name" placeholder="Customer Name" > 
                <label for ="addr"><b>Address</b></label>
                <input type ="text" id = "addr" name="addr" placeholder="Customer Address"> 
                <button class="button" type ="summit" value ="submit" >submit</button>
            </form>
        </div>
    </body>
</html>
