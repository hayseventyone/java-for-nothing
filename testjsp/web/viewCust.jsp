<%-- 
    Document   : viewCust
    Created on : Jun 18, 2019, 11:09:43 PM
    Author     : home
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="model.Customer"%>
<%@page import="javax.annotation.PostConstruct"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri= "http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Display all Customers</h1>
        User : ${sessionScope.user}, <a href="./logout"> logout </a>
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Address</th>
            </tr>
            <c:if test="${sessionScope.allcust ne null }">
                 <c:forEach var="cust" items="${sessionScope.allcust}">
                <tr>
                    <td>${cust.id}</td>
                    
                    <td>${cust.name}</td>
                    
                    <td>${cust.addr}</td>
                </tr>        
            </c:forEach>

            </c:if>
           
            <%
//
//                if (session.getAttribute("allcust") == null) {
//                    List<Customer> cust = new ArrayList<>();
//                } else {
//                    List<Customer> cust = (List<Customer>) session.getAttribute("allcust");
//
//                    for (Customer c : cust) {
//                        out.print("<tr>");
//                        out.print("<td>" + c.getId() + "</td>");
//                        out.print("<td>" + c.getName() + "</td>");
//                        out.print("<td>" + c.getAddr() + "</td>");
//                        out.print("</tr>");
//                    }
//                }
            %>  
        </table> 
    </body>
</html>
<%!
//    private List<Customer> cust = new ArrayList<>();
//
//    @PostConstruct
//    public void initCust() {
//        cust.add(new Customer("1", "one", "thai"));
//        cust.add(new Customer("2", "Two", "thai"));
//        cust.add(new Customer("3", "threee", "brunoma"));
//        cust.add(new Customer("4", "four", "china"));
//    }
%>
