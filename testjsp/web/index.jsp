<%@page import="java.io.IOException"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="javax.annotation.PreDestroy"%>
<%@page import="javax.annotation.PostConstruct"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<%
    Cookie[] cookies = request.getCookies();
    for (Cookie c : cookies) {
        if (c != null && c.getName().equals("user")) {
            session.setAttribute("user", c.getValue());
            break;
        }
    }
%>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>JSP</div>
        <button><a href="./login.html">Login</a>  </button>
        <br>
      
        <br>
        <a href="./DisplayAllCust"> View Customer servlet</a>
        <br>
        <a href="./addNewCustomer.jsp"> New Customer servlet</a>
        
        


    <center>there are <%=++counter%> people visit your website </center>
</body>
</html>

<%!
    int counter = 0;

    @PostConstruct
    public void myLoad() {
        try {
            counter = 0;

            FileReader fr = new FileReader("counter.txt");
            BufferedReader br = new BufferedReader(fr);

            String counterStr = br.readLine();

            counter = Integer.parseInt(counterStr.split("=")[1]);
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    @PreDestroy
    public void saveCounter() {
        try {
            FileWriter fw = new FileWriter("counter.txt");
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write("counter=" + counter);
            bw.close();
            fw.close();

        } catch (IOException e) {
            System.out.println(e);
        }

    }
%>
