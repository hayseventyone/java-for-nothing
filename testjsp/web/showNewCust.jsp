<%-- 
    Document   : showNewCust
    Created on : Jun 21, 2019, 12:31:26 AM
    Author     : home
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Show New Customer</title>
    </head>
    <body>
        <h1> Customer Detail </h1>
        <!--get from newCustController-->
        <%--<jsp:useBean id ="newCust" class="model.Customer" scope="request">--%>
            <%--<jsp:setProperty name ="newCust" property="*"/>--%>
            <%--<jsp:setProperty name ="newCust" property="id" param="id" />--%>
             <%--<jsp:setProperty name ="newCust" property="name" param="name" />--%>
              <%--<jsp:setProperty name ="newCust" property="addr" param="addr" />--%>
        <%--</jsp:useBean>--%>
        
        <!--get from newCustController with id ="newCust" -->
        <jsp:useBean id ="newCust" class="model.Customer" scope="request"/>
        ID : <jsp:getProperty name ="newCust" property="id"/> <br>
        
        Name : <jsp:getProperty name ="newCust" property="name"/> <br>
        
        Address : <jsp:getProperty name ="newCust" property="addr"/> <br>
        
        <a href="./viewCust.jsp"> View All Customer</a>
        
        <%
//      move to controller
//          List<Customer> allCusts = (List<Customer>)session.getAttribute("allcust");
//          if(allCusts == null){
//             allCusts = new ArrayList<Customer>();  
//            
//          }
//          allCusts.add((Customer)request.getAttribute("newCust"));
//          session.setAttribute("allcust", allCusts);
//          System.out.println(allCusts);
        %>
    </body>
</html>
