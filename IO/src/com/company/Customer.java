package com.company;

import java.io.Serializable;

public class Customer implements Serializable {
    private int id;
    private String Name;
    private String Email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Customer(int id, String name, String email) {
        this.id = id;
        Name = name;
        Email = email;
    }
}
