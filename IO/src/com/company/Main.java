package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
	// write your code here
       FileInputStream finp = new FileInputStream("customer.txt");
       ObjectInputStream oinp = new ObjectInputStream(finp);



       List<Customer> custs = (List<Customer>) oinp.readObject();

       oinp.close();
       finp.close();

        for (Customer cust: custs
             ) {
            System.out.println(cust.getEmail());;
            System.out.println(cust.getName());
            System.out.println(cust.getId());
            System.out.println("----------------");

        }




    }

    public  void  writeObj() throws  IOException{
        List<Customer> custs = new ArrayList<>();

        custs.add(new Customer(1,"benz","benz@hotmail.com"));

        custs.add(new Customer(2,"benz2","benz2@hotmail.com"));

        custs.add(new Customer(3,"benz3","benz3@hotmail.com"));


        FileOutputStream fout = new FileOutputStream("customer.txt");

        ObjectOutputStream oout =  new ObjectOutputStream(fout);

        oout.writeObject(custs); //write Object;
        oout.close();
        fout.close();


    }

    public  void readCSV() throws  IOException{
        FileReader reader = new FileReader("file.txt");
        BufferedReader breader = new BufferedReader(reader);

        String read =null;
        List<Customer> custs = new ArrayList<>();
        while ((read = breader.readLine())!=null){

            String[] data = read.split(",");

            Customer cust  = new Customer(Integer.parseInt(data[0]),data[1],data[2]);

            custs.add(cust);

        }
        breader.close();
        reader.close();

        for (Customer cust: custs) {
            System.out.println("name  : "+cust.getName());
            System.out.println("email : "+cust.getEmail());
            System.out.println("ID : "+cust.getId());
            System.out.println("---------------------------");
        }




    }

    public  void Write(String[] args) throws IOException {
                FileOutputStream fout = new FileOutputStream("data.txt");

        BufferedOutputStream buff = new BufferedOutputStream(fout);

        DataOutputStream dataout = new DataOutputStream(buff);

        dataout.writeUTF("hello world");

        dataout.writeInt(12);

        dataout.writeDouble(33.3D);



        dataout.close();
        buff.close();
        fout.close();

        System.out.println("File Done");

    }

    public  void read(String[] args) throws IOException {
        FileInputStream finp = new FileInputStream("data.txt");
        BufferedInputStream binp = new BufferedInputStream(finp);
        DataInputStream dinp = new DataInputStream(binp);


        String d2 = dinp.readUTF();

        int d1    = dinp.readInt();

        double d3 = dinp.readDouble();


        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);

        dinp.close();
        binp.close();
        finp.close();


    }
}
