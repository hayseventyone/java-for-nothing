package com.company;

import java.util.ArrayList;
import java.util.List;

public class UniversityException extends Exception {

    public UniversityException(String message) {
        super(message);
    }
}


 class University implements  AutoCloseable{
    List<Student> students = new ArrayList<>();
    public  int getSize(){
        return students.size();
    }

     @Override
     public void close() {
         students.clear();
     }

     public  void registerNewStudent(Student NewStudent ) throws UniversityException {
       //duplicate
        for (Student std:students) {
            if (std.getName().equals(NewStudent.getName())){
                throw  new  UniversityException("Duplicate");

            }

        }

        //new Student
        students.add(NewStudent);

    }

 }