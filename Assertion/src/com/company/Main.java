package com.company;


public class Main {

    public static void main(String[] args) {
	// write your code here


        Student s1 = new Student();
        s1.setName("bez");

        Student s2 = new Student();
        s2.setName("benz");

        Student s3 = new Student();
        s3.setName("benz");

        University uni2 =null;
        try (University uni = new University()){// use try with Resource and autoclose
             uni2 = uni;
            uni.registerNewStudent(s1);
            System.out.println(uni.getSize());
            uni.registerNewStudent(s2);
            System.out.println(uni.getSize());
            uni.registerNewStudent(s3);//duplicate
            System.out.println(uni.getSize());

        } catch (UniversityException e) {
            System.out.println(e.getMessage());
//            e.printStackTrace();
        }
//        uni.close(); autoclose from interface autocloseable

        System.out.println(uni2.getSize());
    }
}
