public class TaxUtil {
    public static double calVat(double amt){
        return amt  * 0.07;
    }

    public static double calIncludeVat(double amt){
        return amt + calVat(amt);
    }

    public static double calWHT(double amt ){
        return amt * 0.03;
    }
}
