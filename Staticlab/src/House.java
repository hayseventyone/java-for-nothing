public class House {

    public House() {


    }

    public House(String addr, String color) {
        Addr = addr;
        Color = color;
    }

    //
    private String Addr;
    private String Color;

    public String getAddr() {
        return Addr;
    }

    public void setAddr(String addr) {
        Addr = addr;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }
}
