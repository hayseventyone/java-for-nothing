package Customer ;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import Customer.EntityCustomer;



/**
 * CustomerService
 */
@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository repo) {
        this.customerRepository = repo;
    }

   
    public List<EntityCustomer> retriveCustomer() {
        return (List<EntityCustomer>) customerRepository.findAll();
    }

    
    public Optional<EntityCustomer> retriveCustomer(long id) {
        return customerRepository.findById(id);
    }

    
    public EntityCustomer createCustomer(EntityCustomer cust) {
       
        return customerRepository.save(cust);
    }

    
    public Optional<EntityCustomer> updateCustomer(Long id, EntityCustomer cust) {
        Optional<EntityCustomer> customerOptional = customerRepository.findById(id);
        if (!customerOptional.isPresent()) {
            return customerOptional;
        }
        cust.setId(id);
        return Optional.of(customerRepository.save(cust));

    }

   
    public boolean deleteCustomer(Long id) {
        try {
            customerRepository.deleteById(id);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;

        }

    }

}