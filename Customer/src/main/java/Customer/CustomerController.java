package Customer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.Optional;
import Customer.EntityCustomer;

@Controller
@RequestMapping("/api")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

//	@GetMapping(path = "/add")
//	public @ResponseBody String createCustomer(@RequestParam String name, @RequestParam String addr) {
//
//		EntityCustomer cust = new EntityCustomer();
//		cust.setName(name);
//		cust.setAddr(addr);
//		customerRepository.save(cust);
//		return "Saved";
//
//	}
//
	@GetMapping(path = "/customers/all")
	public @ResponseBody Iterable<EntityCustomer> getAllCustomer() {
		return customerService.retriveCustomer();
	}


//	@GetMapping()
//	    public List<EntityCustomer> getAllCust() {
//	        return customerService.retriveCustomer();
//	    }


	@GetMapping("/customers/{id}")
	public ResponseEntity<?> getCustomer(@PathVariable Long id) {
		Optional<EntityCustomer> customer = customerService.retriveCustomer(id);
		if (!customer.isPresent()) {
			return ResponseEntity.notFound().build();

		}
		return ResponseEntity.ok((customer));
	}

	@PostMapping("/customers")
	public ResponseEntity<?> postCustomer(@Valid @RequestBody EntityCustomer cust) {
		EntityCustomer newCustomer = customerService.createCustomer(cust);
		return ResponseEntity.status(HttpStatus.CREATED).body(newCustomer);
	}

	@PutMapping({ "/customers/{id}" })
	public ResponseEntity<?> updateCustomer(@PathVariable long id, @Valid @RequestBody EntityCustomer cust) {
		Optional<EntityCustomer> customer = customerService.updateCustomer(id, cust);
		if (!customer.isPresent()) {
			return ResponseEntity.notFound().build();

		}
		return ResponseEntity.ok().build();
	}

	@DeleteMapping({ "/customers/{id}" })
	public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {
		if (!customerService.deleteCustomer(id)) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().build();
	}

}
