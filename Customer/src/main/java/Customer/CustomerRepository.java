package Customer;

import org.springframework.data.repository.CrudRepository;

import Customer.EntityCustomer;;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CustomerRepository extends CrudRepository<EntityCustomer, Long> {

}
