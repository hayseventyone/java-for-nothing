import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyDAOimpl implements TweetDAO {

    private static String driver = "mongodb://";
    private static String user   = "";
    private static String pass   = "";
    private static String Uri    = "@ds135653.mlab.com:35653/?authSource=tweetme_db&authMechanism=SCRAM-SHA-1";
    private static String db ="tweetme_db";
    private static String collectionMongo ="TweetMe";
    private static MongoDatabase database;


    private static MyDAOimpl instance = new MyDAOimpl();

    private MyDAOimpl(){



    };

    public static MyDAOimpl getInstance(){
        return instance;
    }



    @Override
    public List<Tweet> getAllTweets() {
        List<Tweet> allTweet = new ArrayList<>();
        String connectionStr =  driver+user+':'+pass+Uri;
        try{

            MongoClient client = MongoClients.create(new ConnectionString(connectionStr));
            MongoDatabase database =  client.getDatabase(db);
            ArrayList<Document> collection = database.getCollection(collectionMongo)
                    .find()
                    .into(new ArrayList<Document>());
            for (Document doc: collection) {

                String name = doc.get("name").toString();
                String content = doc.get("content").toString();
                String id  = doc.get("_id").toString();
                Date created =((Date) doc.get("created"));;

                allTweet.add(new Tweet (name,content,created,id));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return allTweet;
    }

    @Override
    public void newTweet(Tweet newTweet) {

    }

    @Override
    public void delete(Tweet delTweet) {

    }

    @Override
    public Tweet getCustomer(String id) {
        return null;
    }
}
