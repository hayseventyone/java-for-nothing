import model.Customer;

import java.util.List;

public interface InfCustomerDAO {
    public List<Customer> getAllCustomer();
    public void addCustomer(Customer NewCust);
    public void updateCustomer(Customer old ,Customer newCust);
    public void deleteCustomer(Customer old ,Customer newCust);
    public Customer getCustomer(String id);
}
