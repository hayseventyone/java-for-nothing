import java.util.List;

public interface TweetDAO {
    public List<Tweet> getAllTweets();
    public void newTweet(Tweet newTweet);
    public void delete(Tweet delTweet);
    public Tweet getCustomer(String id);

}
