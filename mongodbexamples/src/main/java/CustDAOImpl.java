import model.Customer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustDAOImpl implements InfCustomerDAO {

        private static CustDAOImpl instance = new CustDAOImpl();

        private static final String drivername = "com.mysql.jdbc.Driver";
        private static final String url =  "jdbc:mysql://localhost:3306/testdb?verifyServerCertificate=false&useSSL=false"; //jdbc:mysql://localhost:3306/testdb?autoReconnect=true&useSSL=false";

        private static final String user = "root";
        private static final String passward = "bz1234";

        private CustDAOImpl() {
            try {
                Class.forName(drivername);
                System.out.println("connect");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CustDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public static CustDAOImpl getInstance() {
            return instance;
        }

        private static final String GET_ALL_CUSTOMER = "SELECT * FROM Customer ;";
        private static final String FIND_BY_ID = "SELECT * FROM Customer where id ? ;";
        private static final String DELETE_CUSTOMER = "DELETE FROM Customer where id ? ;";
        private static final String UPDATE_CUSTOMER = "UPDATE Customer Set name =? ,addr = ? where id =?";
        private static final String ADD_NEW_CUSTOMER = "INSERT INTO Customer VALUES(?,?,?) ;";

        @Override
        public List<Customer> getAllCustomer() {
            List<Customer> allCust = new ArrayList<>();
            try {
                Connection con = DriverManager.getConnection(url, user, passward);
                System.out.println("connect");
                Statement stm = con.createStatement();
                ResultSet res = stm.executeQuery(GET_ALL_CUSTOMER);

                while (res.next()) {
                    String id = res.getString("id");
                    String name = res.getString("name");
                    String addr = res.getString("addr");
                    allCust.add(new Customer(id, name, addr));
                }

                res.close();
                stm.close();
                con.close();
                return allCust;
            } catch (SQLException ex) {
                Logger.getLogger(MyDAOimpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            allCust.clear();
            return allCust;

        }

        @Override
        public void addCustomer(Customer NewCust) {

            try {
                Connection con = DriverManager.getConnection(url, user, passward);
                PreparedStatement stm = con.prepareStatement(ADD_NEW_CUSTOMER);
                stm.setString(1, NewCust.getId());
                stm.setString(2, NewCust.getName());
                stm.setString(3, NewCust.getAddr());
                int res = stm.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(CustDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        @Override
        public void updateCustomer(Customer old, Customer newCust) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void deleteCustomer(Customer old, Customer newCust) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public Customer getCustomer(String id) {

            try {
                Connection con = DriverManager.getConnection(url, user, passward);
                PreparedStatement stm = con.prepareStatement(FIND_BY_ID);
                stm.setString(1, id);
                ResultSet res = stm.executeQuery();

                if (res.first()) {
                    return new Customer(res.getString(1), res.getString(2), res.getString(3));

                }
                res.close();
                stm.close();
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(CustDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;

        }

    }
