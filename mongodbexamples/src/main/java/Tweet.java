import java.util.Date;

public class Tweet {

    private String name;
    private String content;


    private Date created;
    private String _id;

    public Tweet(String name, String content, Date created, String _id) {
        this.name = name;
        this.content = content;
        this.created = created;
        this._id = _id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
