package com.company;

import static java.lang.Thread.sleep;

public class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 10 ; i++) {
            System.out.println("Hello :® "+(i+1)+" " +Thread.currentThread().getName());
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
