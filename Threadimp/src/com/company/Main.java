package com.company;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) {
	// write your code here

        MyThread t1 = new MyThread("Thai");

        MyThread t2 = new MyThread("China");

        MyThread t3 = new MyThread("Pinoy");


        t1.start();
        t2.start();
        t3.start();

        MyRunnable r = new MyRunnable();
        Thread t4 = new Thread(r);
        Thread t5 = new Thread(r);
        Thread t6 = new Thread(r);
        Thread t7 = new Thread(r);

        t4.start();
        t5.start();
        t6.start();
        t7.start();

        new Thread(r).start();

        Runnable rr = () -> { for (int i = 0; i < 10 ; i++) {
            System.out.println("Hello :xx "+(i+1)+" " +Thread.currentThread().getName());
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }};

        rr.run();


    }
}
