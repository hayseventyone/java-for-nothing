public class MyCircle extends MyGraphics {
    private double radius;

    @Override
    public void draw() {
        //logic r*r = x*x+y*y
    }

    public MyCircle() {
    }

    public MyCircle(double radius) {
        this.radius = radius;
    }

    public MyCircle(int x, int y, double radius) {
        super(x, y);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double findArea() {

        return (22.0/7.0)*radius*radius;
    }
}
