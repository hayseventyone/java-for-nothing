public abstract class MyGraphics {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public MyGraphics() {
    }

    public MyGraphics(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public abstract void draw() ; //abstract method
    public abstract double findArea();

    public void move(int x ,int y ){ //Concreate method
        this.setX(x);
        this.setY(y);
    }

}
