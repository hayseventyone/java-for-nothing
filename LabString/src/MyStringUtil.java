import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyStringUtil {
    public static String rmSpace(String str){
        if (str == null || (str.isEmpty())){return "";}
        String res ="" ;
        return  res  = str.trim().replaceAll(" +", "_");



    }

    public static int count(String input ,String word){
        if (input == null || input.isEmpty()) { return 0; }

       int cnt = 0;
        Pattern P =  Pattern.compile(word);
        Matcher M =  P.matcher(input);
        while (M.find()){
            cnt++;
        }

        return  cnt;
    }

    public static String replace(String src,String from,String with){
        return src.replace(from,with);

    }

}
